// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('visits the app root url', () => {
    cy.visit('/')
    cy.contains('h1', 'The adder')

    const number = `${Math.floor(Math.random() * 100000000)}`, number1 = `${Math.floor(Math.random() * 10000000)}`

    cy.get('#app > input:first-of-type').clear().type(number)
    cy.get('#app > input:nth-of-type(2)').clear().type(number1)

    cy.get('#app > span').contains(number+number1)
  })
})
