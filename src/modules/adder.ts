/**
 adds two numbers
 @param {number} number first number
 @param {number1} number second number
 @returns {number} the sum of both
 */
export function adder(number: number, number1: number): number {
    return number + number1
}