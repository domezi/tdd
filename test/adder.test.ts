import {assert, expect, test} from 'vitest'
import {adder} from "../src/modules/adder";

test('Adder', () => {
    const a: number = Math.floor(Math.random() * 50), b: number = Math.floor(Math.random() * 50)
    expect(adder(1, 2)).eq(3)
    expect(adder(a, b)).eq(a + b)
})
